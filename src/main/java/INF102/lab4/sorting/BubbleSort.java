package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        int n = list.size();
        do {
            int newN = 0;
            for (int i = 1; i < n; i++){
                T first = list.get(i-1);
                T second = list.get(i);
                if(first.compareTo(second) > 0){
                    list.set(i-1, second);
                    list.set(i, first);

                    newN = i;
                }
            }
            n = newN;
        }while(!(n <= 1));
    }
    
}
