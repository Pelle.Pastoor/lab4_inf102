package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickMedian implements IMedian {

    Random rnd = new Random();

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list);
        
        return find_median(listCopy, listCopy.size() / 2);
    }

    private <T extends Comparable<T>> T find_median(List<T> list, int index){
        if(list.size()<=1)
			return list.get(0);

		T pivot = list.get(rnd.nextInt(list.size()));
		List<T> first = new ArrayList<>();
		List<T> pivots = new ArrayList<>();
		List<T> last = new ArrayList<>();
		for(T t : list) {
			int cmp = t.compareTo(pivot);
			if(cmp<0) {
				first.add(t);
			}
			else {
				if(cmp>0) {
					last.add(t);
				}
				else {
					pivots.add(t);
				}
			}
		}

        if(index < first.size()){
            return find_median(first, index);
        }else if(index < first.size() + pivots.size()){
            return pivot;
        }else {
            return find_median(last, index - first.size() - pivots.size());
        }
    }

}
